# Solid Storage

If you are looking for a model to store granular solid material (e.g., regolith), a Hopper model can be found [here](./ISRULib/10_Handling/00_Solids_Handling/Hopper/Hopper.ipynb).
