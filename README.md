# ISRULib

This is the ISRULib Gitlab project. For more detailed information visit the [Wiki documentation site](https://wiki.tum.de/display/lpe/ISRULib) and its [project website](https://www.asg.ed.tum.de/en/lpe/research/production-of-oxygen-water-and-metals/isrulib/). 

The goal is to keep ISRULib alive and constantly updated. To this end, if you have any inquiries or want to collaborate to expand ISRULib by including your models in the database, please contact **Francisco J. Guerrero-Gonzalez ([f.guerrero@tum.de](mailto:f.guerrero@tum.de))**.

## Software Requirements

ISRULib models are written in [Python 3.0](https://www.python.org/download/releases/3.0/) making use of [Jupyter Notebooks](https://jupyter.org/), which is a web-based interactive computing platform that combines live code, equations, narrative text, visualizations, etc. In this way, readable and easily-understandable ISRU models can be built. A simple way to utilize ISRULib is to download the open-source [Anaconda](https://www.anaconda.com/) package and environment management system, which already includes [Python 3.0](https://www.python.org/download/releases/3.0/) and [Jupyter Notebooks](https://jupyter.org/).

## How to Create a New Model
There are few code guidelines to create a new model for ISRULib. A Jupyter Notebook template is provided in [Template.ipynb](./ISRULib/99_Template/Template.ipynb). The  models will be subject to a peer review before being finally included in ISRULib.

The general structure of the model will be exemplified with the currently available [HRI Fluidized Bed Reactor](./ISRULib/30_Regolith_Processing/00_Hydrogen Reduction of Ilmenite/HRI_Fluidized_Bed_Reactor/HRI Fluidized Bed Reactor.ipynb) model. The model must include:

A code cell where the author and reviewer names as well as the model version are stated.

```python:
# author: Francisco J. Guerrero-Gonzalez (f.guerrero@tum.de)
# reviewer: Francisco J. Guerrero-Gonzalez
# version: 1.0.0
```

A markdown cell where the model license is included.

```markdown:
Copyright 2023, Professorship of Lunar and Planetary Exploration, Technical University of Munich.

Permission is hereby granted, free of charge, to any person obtaining a copy of this model and associated documentation files, without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the models, and to permit persons to whom the model is furnished to do so, subject to the following conditions:

1. The above copyright notice and this permission notice shall be included in all copies or substantial portions of the model, as well as reference to the author(s) listed above. Scientific publications which use this model shall cite the [ISRULib documentation site](https://wiki.tum.de/display/lpe/ISRULib) attributing credit to the named authors in the documentation.

2. The model is provided without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose, and non-infringement. In no event shall the authors or copyright holders be liable for any claim, damages, or other liability, whether in an action of contract, tort, or otherwise, arising from, out of, or in connection with the model or the use or other dealings in the model.
```
A markdown cell where the component name or title is stated.

```markdown:
# HRI Fluidized Bed Reactor
```

A code cell with the necessary Python libraries and dependencies.

```python:
# Python Libraries
import numpy as np
from IPython import display
from scipy.optimize import fsolve
```
A markdown cell that includes a brief description of the model.

```markdown:
**Model Description**

Hydrogen reduction is a conceptually simple process that reduces solid FeO-rich regolith minerals, such as ilmenite (FeTiO3), when in contact with a hydrogen flow at moderate temperatures of 1000–1300 K. HRI does not only reduce iron oxides contained in ilmenite grains, but also FeO that might be found dispersed in glass-phase regolith particles. The reduction reaction yields solid iron and water vapor.

$FeO_{(s)} + H_{2(g)} \rightarrow Fe_{(s)} + H_2O_{(g)}$

Water might be electrolyzed into oxygen and hydrogen, recirculating hydrogen back into the HRI reactor for further reduction. This model considers the fluidized bed reactor highlighted in blue in the figure below. A fluidized bed reactor is a type of reactor device where a fluid (hydrogen gas in this case) is passed through a solid granular material (regolith) at high enough speeds to suspend the solid and cause it to behave as though it were a fluid, facilitating heat transfer and the reaction kinetics.
```

A descriptive figure of the component or the model. The figure must be placed in the [figs folder](./ISRULib/99_Template/figs/).
```python:
# Include here a descriptive figure of the component (model)
display.Image("figs/HRI_reactor.png")
```

A markdown cell that summarizes the model assumptions, simplifications and proposes future improvements to the model.

```markdown:
**Model Assumptions, Simplifications and future Improvements**

The model inputs regolith at an environment temperature and heats it to an operating temperature. It also inputs hydrogen at the boiling temperature of water and heats it to an operating temperature. The model outputs water vapor and hydrogen at the operating temperature and pressure of the reactor.

An excess of hydrogen is necessary to fluidize the reactor (Linne et al., 2010) and to maintain the outlet water partial pressure below 10% (Sargeant, 2020) to not hinder the reduction. Therefore, the inlet hydrogen flow is calculated taking into consideration these two conditions.

The HRI kinetics model is based on the work carried out at the NASA John H. Glenn Research Center (Hedge et al., 2009, 2010, 2011; Linne et al., 2010). They formulated an empirical model from hydrogen reduction runs of JSC-1A simulants, which included the following parameters to determine the conversion rate of FeO contained in the simulant: operating temperature, operating pressure, FeO content in regolith, and reduction time.

The model considers 3 process phases: heating, reduction and downtime. At least two reactors (always an even number) operate simultaneously. One reactor heats a regolith batch while the other one reduces a subsequent batch. This concept of operations reduces the necessary peak power (Linne et al., 2010).

The models assumes a fluidized bed reactor that operates in batches. The reactor is modeled as a hollow Inconel cylinder covered by a MLI-like insulation to reduce the heat losses to the environment.

This model can be optimized (e.g., iteratively) for certain inputs presented in **1. Inputs** with the goal of reducing the reactor mass and/or power.

Since hydrogen is introduced in the reactor at the boiling temperature of water, the model assumes that after the reduction, the outlet gas stream (hydrogen and water) will be cooled down, condensating water and introducing this into a PEM electrolyzer (see figure above). However other options are available (e.g., solid-state electrolysis), which would change the temperature from which hydrogen has to be heated. Moreover, heat recuperation devices such as heat exchangers between the outlet and inlet gas flows or concentric regolith heaters with the reactor (Linne et al., 2010) could be included to reduce the necessary power.

The reactor mass model does not include the necessary heating devices to elevate the temperature of hydrogen and regolith. This has to be computed externally.

Future improvements could include:
- Incorporating the average particle size as an input for the calculation of the HRI kinetics (parameter F)
- Improving the dependency of the the HRI kinetics (parameter F) with the pressure. When iteratively optimizing the model for a certain pressure, the non-continuous dependency point at 1 bar means that often this pressure is the optimum
- Modifying the downtime based on reactor size and the feeding rate instead of being a fixed amount of time
- Investigating the scalability limitations of the reactor size (diameter) due to fluidization issues. It might be necessary to include an upper limit for the reactor diameter in case that, after a certain diameter, the fluidization formulas provided by Linne et al. (2010) do not hold anymore.

The rest of the assumptions and possible improvements are explained thoroughly in **3. Model Body**
```

After the introductory cells, the actual model code begins. The model code is organized in 4 cells.
1. Inputs
2. Constants
3. Model Body
4. Outputs

The most important remarks are that:
- The variable names must be easily understandable. Long and self-explanatory variable names are preferable to short variable names. E.g., ```reactor_heating_losses``` is preferable than ```Q_losses```.
- Each newly defined variable must include its units and a brief description of the variable as a comment. E.g., ```gMoon = 1.625 # m/s2 | Gravitational acceleration on the Moon```.

For further clarification, see the examples below.

The **"1. Inputs"** cell includes the varaibles that can be modified by the user.

```python:
yearly_regolith_input = 200 # t/a | Regolith input into the reactor in tons per year

n_reactors = 4 # - | Number of reactors. It must always be an even number (2, 4, 6, 8, 10, etc.)

T_operation = 1000 # K | HRI Operating temperature. It can be varied between 1000 K and 1300 K based on Hedge et al. (2009)'s F-fit
p_operation = 1e5 # Pa | HRI Operating pressure HRI. It has to be lower than 3 bar based on Hedge et al. (2011)

FeO_concentration = 25 # wt.% | iron oxide (FeO) concentration of the introduced regolith. Can be obtained from the reducable regolith material (e.g. ilmenite (FeTiO3), glass)

HRI_conversion_rate = 0.8 # - | Amount of ilmenite that reacts with hydrogen from the total amount of ilmenite that is in a single reactor batch

H_D_ratio = 3 # - | H/D (Ratio between the reactor hegiht and diameter)
```
The **"2. Constants"** cell includes the variables that cannot be modified by the user. E.g., universal constants, material properties, etc.

```python:
e = np.e # -
pi = np.pi # -
sigma = 5.67e-8 # W/(m2-K4) | Stefan-Boltzman constant

gMoon = 1.625 # m/s2 | Gravitational acceleration on the Moon

T_start_red = 900 # K | Minimum temperature for reduction to start. Hedge et al. (2010) state that it can range from 873 K to 923 K
downtime_time = 5*60 # s | Time to load and unload regolith batches. Rough estimation based on Kaschubek et al. (2021)
duty_cycle = 0.5 # - | Amount of time the reactor is operating per year

average_particle_size = 93*1e-6 # m | Average diameter of JSC-1A simulant from Linne et al. (2010)
JSC_1A_particle_density = 2915 # kg/m3 | Average particle density of JSC-1A simulant from Meurisse et al. (2017)

reactor_volume_ratio = 2 # - | ratio between the total reactor volume and the batch volume. Linne et al. (2010)
# defines a reactor volume ratio of 3 to fully fluidize the batch. However,
# introducing hydrogen in a pulsed manner can reduce this ratio.

T_environment = 273 # K | Average planetary surface temperature and temperature at which regolith is introduced in the reactor 
T_outer_space = 3 # K | Temperature of outer space

R_h2 = 4124 # J/kgK | Ideal gas constant of hydrogen
R_h2o = 462 # J/kgK | ideal gas constant of water
mH2 = 2 # g/mol | Molecular mass of hydrogen
mH2O = 18 # g/mol | Molecular mass of water
mFeO = 72 # g/mol | Molecular mas of iron oxide

rhoRegolith = 1500 # kg/m3 | Bulk density of regolith (Schreiner et al., 2016)
rhoInconel = 8220 # kg/m3 | Density of Inconel

safety_factor = 10 # - | Structural safety factor for the reactor walls
design_stress_Inconel = 0.40*1034 # MPa at 1000 K | Maximum allowable stress of Inconel (Eagle Inc., 1988)
min_tReactor = 2e-3 # m | Mininum allowable reactor wall thickness
```

The **"3. Model Body"** cell includes the core model code. The cell can be split into multiple cells intercalated with markdown text to further clarify parts of the code. It can also be divided into subsections for better understanding. Longer explanatory comments are also welcome.

```python:
# HRI Kinetics
# Ilmenite start to react with H2 while being heated before reaching the operating temperature (T_operation) (Hedge et al., 2010)
if T_operation > T_start_red:
    avg_T_operation = (T_operation-T_start_red)/np.log(T_operation/T_start_red) # K | Average temperature at which the reduction takes place (Eq. 8 from Hedge et al. (2010))
else:
    avg_T_operation = T_operation # K | Average temperature at which the reduction takes place

# Parameter F comes from Hedge et al. (2009) to represent the kinetics of HRI
parameter_F = e**(-10761/avg_T_operation - 2.8808) # 1/s | Eq. 13 from Hedge et al. (2009)
# The parameter F was fitted for the JSC-1A simulant, which has a "constant" FeO content and average particle size
# Moreover, the reduction process was run under constant pressure and temperature conditions
# This parameter might be linearly corrected for an alternative FeO content.
# Similarly it could be done for the average particle size.
parameter_F_corr = parameter_F*(FeO_concentration/11.2) # 1/s | Corrected parameter F by Eq. 11 from Hedge et al. (2009). 11.2 is the FeO wt.% present in the JSC-1A simulant 
# A dependency of parameter F with pressure can also be included
if p_operation >= 1e5:
    parameter_F_corr = parameter_F_corr*(p_operation/101325)**(1-0.63) # 1/s | Corrected parameter F by Eq. 1 and 9 from Hedge et al. (2011), which are applicable between 1 and 3 atm
else:
    parameter_F_corr = parameter_F_corr*(p_operation/101325) # 1/s | Corrected parameter F by Hedge et al. (2011)

# HRI time
reduction_time = -(3*(1-HRI_conversion_rate)**(2/3) - 2*(1-HRI_conversion_rate) - 1)/parameter_F_corr # s | HRI reduction time (Eq. 12 from Hedge et al. (2009))
# The model considers 3 process phases: heating, reduction and downtime.
# The reduction and heating phases occur simultaneously in 2 different reactors following the analyses of Linne et al. (2010)
heating_time = reduction_time # s
batch_time = heating_time + reduction_time + downtime_time # s

# HRI batches
n_batches  = 365*24*3600*duty_cycle/(n_reactors*batch_time) # -/a | Number of reactor batches per year
batch_mass = yearly_regolith_input*1e3/(n_reactors*n_batches) # kg | Regolith mass per reactor batch

# Fluidized bed reactor geometry
# The reactor is simply modeled as a cylinder. Linne et al. (2010) state:
# The height of the reactor should be 3 times the height of the regolith batch. Therefore, for a constant diameter,
# the volume of the reactor should be 3 time the volume of the regolith batch to fully fluidize it.
# When hydrogen gas is introduced into the reactor in a pulsed manner, the reactor appears to provide
# a large enough plenum volume such that the flow becomes more steady-state within the chamber. 
# The reactor height can then be much smaller as it is dependent on the velocity in the upper portion of the reactor.
reactor_inner_volume = reactor_volume_ratio*batch_mass/rhoRegolith # m3 | Reactor inner volume
reactor_diameter = (4*reactor_inner_volume/(pi*H_D_ratio))**(1/3) # m | Reactor diameter
reactor_height   = H_D_ratio*reactor_diameter # m | Reactor height
# The reactor wall material is assumed to be Inconel. To calculate the necessary reactor wall thickness, 
# the maximum required wall thickness to withstand the following pressures inside the reactor is chosen:
# (1) withstand gas pressure inside the reactor
# (2) withstand regolith batch weight before fluidization (calculated as the hydrostatic pressure of a fluid)
tReactor_gasPressure = safety_factor*p_operation*1e-6*reactor_diameter/(2*design_stress_Inconel) # m | (Eagle Inc., 1988) for fluidized bed reactor
hydrostatic_pressure = reactor_diameter*rhoRegolith*gMoon # Pa | to withstand regolith weigth before fluidization
tReactor_hydroPressure = safety_factor*hydrostatic_pressure*reactor_diameter/(2*design_stress_Inconel*1e6) # m | ASTM D (1998), https://tanks.polyprocessing.com/hubfs/documents/ASTM-D-1998-15.pdf
# A mininum reactor wall thickness is defined
if max(tReactor_gasPressure, tReactor_hydroPressure) < 5e-3:
    tReactor = min_tReactor # m
else:
    tReactor = max(tReactor_gasPressure, tReactor_hydroPressure) # m

## Fluidized bed reactor(s) mass
reactor_shell_volume = 2*pi*tReactor*(0.5*reactor_diameter+tReactor)**2 + reactor_height*pi*((0.5*reactor_diameter + tReactor)**2 - (0.5*reactor_diameter)**2) # m3 | Shell volume of the reactor
reactor_mass = n_reactors*reactor_shell_volume*rhoInconel # kg | Reactor mass. The insulation mas is neglected in this calculation

# Regolith heating power
# It is assumed that the minimum temperature for reduction to start (T_start_red) is reached during the heating time (heating_time) 
# This power calculated as ∫Cp(T)dT between T_environment & T_start_red
# For it, the heat capacity (Cp) of Highlands regolith is considered: Cp (J/(kg-K)) = 2388 - 6794*T**(-0.2508) (Schreiner et al., 2016)
integral_CpdT = (2388*T_start_red - (6794/0.7492)*T_start_red**0.7492) - (2388*T_environment - (6794/0.7492)*T_environment**0.7492) # J/kg | ∫Cp(T)dT
integral_CpdT = integral_CpdT/3600 # Wh/kg | ∫Cp(T)dT
# Afterwards, the temperature is raised from T_start_red to T_operation during the reduction time (reduction_time)
# Therefore, to calculate the final heating power, an additional factor (T_operation-Tenv)/(T_start_red-T_environment) is included to account for the heating power necessary during the reduction phase
regolith_heating_power = batch_mass*integral_CpdT*((T_operation-T_environment)/(T_start_red-T_environment))/(heating_time/3600) # W | Regolith heating power

# Hydrogen heating power
# First the inlet/outlet hydrogen flow has to be calculated. For it, the maximum required hydrogen flow for the following conditions is chosen:
# (1) To fully fluidize the regolith batch (equations from Linne et al., 2010)
# (2) To maintain a water partial pressure below 10% to not hinder the reduction reaction (Sargeant, 2020) 

# Inlet hydrogen flow to fully fluidize the regolith batch (equations from Linne et al., 2010)
epsilon = 2/3 # - | Void fraction
epsilonmf = epsilon # - | Approximation
h2_viscosity = 1.8e-5 # Pa·s | Hydrogen viscosity neglecting the temperature and pressure dependence
h2_density = p_operation/(R_h2*T_operation) # kg/m3 | Hydrogen density using the ideal gas law
# Solve implicit Linne et al. (2010) equation for the hydrogen inlet flow velocity
def func(h2_velocity): # m/s | Hydrogen inlet flow velocity. The material properties (average particle density and size) of JSC-1A simulant are used. Other simulants or actual regolith propoerties could also be used. 
    return(
    h2_velocity - (JSC_1A_particle_density-h2_density)/(150*h2_viscosity)*gMoon*average_particle_size**2*((0.05/h2_velocity)**2*epsilonmf**3)/(1-epsilon) # m/s
    )
h2_velocity = fsolve(func, [0.25])[0] # m/s | Hydrogen inlet flow velocity.
# By injecting half the flow rate (h2_velocity) normally required for fluidization in pulses with equal on-off times,
# the injection velocity (h2_velocity) should be similar to that for the full flow on continuously and sufficient
# to provide good fluidization (Linne et al., 2010). Therefore, the inlet hydrogen flow is:
inlet_h2_flow = h2_density*pi*0.25*reactor_diameter**2*h2_velocity*0.5 # kg/s | Inlet hydrogen flow

# Outlet hydrogen flow to maintain a water partial pressure below 10% to not hinder the reduction reaction (Sargeant, 2020)
outlet_h2o_flow = batch_mass*FeO_concentration*1e-2*(mH2O/mFeO)*HRI_conversion_rate/reduction_time # kg/s | Produced water flow by the reaction FeO + H2 -> Fe + H2O 
outlet_h2_flow = inlet_h2_flow - batch_mass*FeO_concentration*1e-2*(2*mH2/mFeO)*HRI_conversion_rate/reduction_time # kg/s | Outlet hydrogen flow calculated as the inlet hydrogen flow minus the consumed hydrogen by the reaction FeO + H2 -> Fe + H2O
# Water and hydrogen partial pressures
pH2O_pH2 = (outlet_h2o_flow/outlet_h2_flow)*(R_h2o/R_h2) # - | Ratio between water and hydrogen partial pressure (pH2O/pH2)
pH2 = p_operation/(1+pH2O_pH2) # Pa | Hydrogen partial pressure
pH2O = p_operation - pH2 # Pa | Water partial pressure
# Check the condition of water partial pressure being lower than 10% to not hinder the reduction reaction (Sargeant, 2020).
# If the water partial pressure is higher, the inlet hydrogen flow is recalculated (increased) to fulfill the condition
if pH2O/p_operation > 0.1:
    pH2O = 0.1*p_operation # Pa | Limit water partial pressure
    pH2 = p_operation - pH2O # Pa | Limit hydrogen partial pressure
    outlet_h2_flow = outlet_h2o_flow*(R_h2o/R_h2)*(pH2/pH2O) # kg/s | Limit outlet hydrogen flow 
    inlet_h2_flow = outlet_h2_flow + batch_mass*FeO_concentration*1e-2*(2*mH2/mFeO)*HRI_conversion_rate/reduction_time # kg/s | Inlet hydrogen flow calculated as the outlet hydrogen flow minus the consumed hydrogen by the reaction FeO + H2 -> Fe + H2O

# To calculate the hydrogen heating power once the inlet hydrogen flow is known, it is assumed that hydrogen has to be
# heated up from the boiling point of water. This would be true if, after the reduction, PEM electrolysis is carried out.
# If solid state electrolysis is carried out, other starting temperature point is expected. Moreover, it is neglected that
# the stored hydrogen has a different temperature than the cooled down recirculated hydrogen.
# Boiling point of water (Yaws & Satyro, 2015):
A = 8.05573; B = 1723.6425; C = 233.08
Tboil_H2O = -C - B/(np.log10(pH2O/133.3) - A) + 273.15 # K | Boiling point of water (Yaws & Satyro, 2015)
# The hydrogen heating power calculated as ∫Cp(T)dT between Tboil_H2O & T_operation
# For it, the heat capacity (Cp) of hydrogen: Cp (J/mol) is calculated using the Shomate equation Cp = A + B*T + C*T**2 + D*T**3 + E/T**2 (NIST - https://webbook.nist.gov/cgi/cbook.cgi?ID=C1333740&Mask=1&Type=JANAFG&Table=on#JANAFG) 
A = 33.066178; B = -11.363417/1000; C = 11.432816/1000**2; D = -2.772874/1000**3; E = -0.158558*1000**2
integral_CpdT = (A*T_operation + 0.5*B*T_operation**2 + (1/3)*C*T_operation**3 + 0.25*D*T_operation**4 - E/T_operation) - (A*Tboil_H2O + 0.5*B*Tboil_H2O**2 + (1/3)*C*Tboil_H2O**3 + 0.25*D*Tboil_H2O**4 - E/Tboil_H2O) # J/mol | ∫Cp(T)dT
integral_CpdT = integral_CpdT*1e3/3600/mH2 # Wh/kg | ∫Cp(T)dT
hydrogen_heating_power = inlet_h2_flow*3600*integral_CpdT # W | Hydrogen heating power
# Heat recuperation mechanisms are not included in this model. This model could be coupled with a heat exchanger to use
# the hot outlet water + hydrogen flow to partially heat up the inlet hydrogen flow until both flow temperatures are
# equal as proposed by Linne et al. (2010). 50% heat recuperation from the outlet flow should be the theoretical limit.

# Reactor heat losses to the environment
# A simplified thermal model is used to calculate the reactor heat losses to the environment
# The reactor radiates 50% of the heat to the lunar surface and 50% to outer space (3 K) -> View factor of 0.5
# No conduction between the reactor and the lunar surface is considered
view_factor = 0.5 # - | View factor to the lunar surface. The reactor radiates 50% of the heat to the lunar surface and 50% to outer space
# The Inconel walls are covered by 10 layers of a MLI-like material capable of withstanding the reactor temperatures 
# without melting. As a simplification, the thermal properties of MLI are considered:
eKapton = 0.5 # - | emissivity for Kapton (Gilmore, 2002)
eMLI = 0.005 # - | effective emissivity for 10 layers (Gilmore, 2002)
reactor_surface = pi*(reactor_diameter+2*tReactor)*reactor_height + 0.5*pi*(reactor_diameter+2*tReactor)**2 # m2 | outer reactor surface
# The thermal effect of the Inconel shell in the temperature drop is neglected (only approximately 5 mm)
# A thermal balance betweem the inner and outer surface is carried out. As a simplification the inner wall is assumed to be
# constantly at the operating temperature even during the heating phase (T_operation).
T_MLI_out = ((eMLI*T_operation**4 + eKapton*view_factor*T_environment**4 + eKapton*(1-view_factor)*T_outer_space**4)/(eKapton+eMLI))**(1/4) # K | Outer surface temperature of thr MLI-like insulation
reactor_heating_losses = sigma*eMLI*reactor_surface*(T_operation**4-T_MLI_out**4)*n_reactors # W | Reactor heat losses to the environment

# Endothermic power (to carry out the reduction reaction FeO + H2 -> Fe + H2O)
# The reduction reaction FeO + H2 -> Fe + H2O is endothermic
enthalphy_h2_reduction = 11e3 # J/mol | approx reduction enthalphy at 1000 K (Yoshida et al., 2000)
enthalphy_h2_reduction = enthalphy_h2_reduction*1e3/3600/mH2O # Wh/kg
endothermic_heating_power = outlet_h2o_flow*enthalphy_h2_reduction*3600/HRI_conversion_rate # W | Endothermic heating power

# Total reactor power
reactor_power = endothermic_heating_power + reactor_heating_losses + hydrogen_heating_power + regolith_heating_power # W | Total reactor power

# Oxygen Production (t/a)  
yearly_oxygen_output = outlet_h2o_flow*reduction_time*n_reactors*n_batches/1e3 # t/a | Oxygen output from the reactor in tons per year
```
The **"4. Outputs"** cell usually prints the mass and power estimations as well as other performance variables that might be necessary.

```python:
print("Mass:          %.2f kg"%(reactor_mass))
print("Power:         %.2f kW"%(reactor_power/1e3))
print("O2 Production: %.2f t/a"%(yearly_oxygen_output))
```

Finally, a **"Reference"** markdown cell is included to collect the referneced literature. In the markdown text or commets, literature references can be cited as, e.g., ```# (Doe et al., 1899)```.

```markdown:
Eagle Engineering Inc, Conceptual Design of a Lunar Oxygen Pilot Plant: LunarBase Systems Study, Tech. Rep. N89-13886, NASA, 1988, URL https://ntrs.nasa.gov/citations/19890004515

D.G. Gilmore, Spacecraft thermal control handbook, Volume I: Fundamental technologies, 2002

U. Hegde, R. Balasubramaniam, S. Gokoglu, Development and validation of a model for hydrogen reduction of JSC-1A, in: 47th AIAA Aerospace Sciences Meeting Including the New Horizons Forum and Aerospace Exposition, American Institute of Aeronautics and Astronautics, Reston, Virigina, 2009, http://dx.doi.org/10.2514/6.2009-1389

U. Hegde, R. Balasubramaniam, S. Gokoglu, Heating-rate coupled model for hydrogen reduction of JSC-1A, in: 48th AIAA Aerospace Sciences Meeting Including the New Horizons Forum and Aerospace Exposition, American Institute of Aeronautics and Astronautics, Reston, Virigina, 2010, http://dx.doi.org/10.2514/6.2010-1546

U. Hegde, R. Balasubramaniam, S. Gokoglu, K. Rogers, M. Reddington, L. Oryshschyn, Hydrogen reduction of lunar regolith simulants for oxygen production, in: 49th AIAA Aerospace Sciences Meeting Including the New Horizons Forum and Aerospace Exposition, American Institute of Aeronautics and Astronautics, Orlando, Florida, 2011, http://dx.doi.org/10.2514/6.2011-608

D. Kaschubek, M. Killian, L. Grill, System analysis of a Moon base at the south pole: Considering landing sites, ECLSS and ISRU, Acta Astronaut. 186 (2021) 33–49, http://dx.doi.org/10.1016/j.actaastro.2021.05.004

D. Linne, Employing ISRU models to improve hardware designs, in: 48th AIAA Aerospace Sciences Meeting Including the New Horizons Forum and Aerospace Exposition, American Institute of Aeronautics and Astronautics, Reston, Virigina, 2010, http://dx.doi.org/10.2514/6.2010-800

A. Meurisse, J.C. Beltzung, M. Kolbe, A. Cowley, M. Sperl, Influence of mineral composition on sintering lunar regolith, Journal of Aerospace Engineering 30 (4) (2017) 04017014, http://dx.doi.org/10.1061/(asce)as.1943-5525.0000721  
    
S.S. Schreiner, J.A. Dominguez, L. Sibille, J.A. Hoffman, Thermophysical property models for lunar regolith, Adv. Space Res. 57 (5) (2016) 1209–1222, http://dx.doi.org/10.1016/j.asr.2015.12.035

H.M. Sargeant, Water from Lunar Regolith: Reduction by Hydrogen for a Smallscale Demonstration of in situ Resource Utilisation for the Moon (Ph.D. thesis), The Open University, 2020, http://dx.doi.org/10.21954/ou.ro.00011fb6

L. Schlüter, A. Cowley, Review of techniques for In-Situ oxygen extraction on the moon, Planet. Space Sci. 181 (2020) 104753, http://dx.doi.org/10.1016/j.pss.2019.104753

C.L. Yaws, M.A. Satyro, Vapor pressure – inorganic compounds, in: The YawsHandbook of Vapor Pressure, Elsevier, 2015, pp. 315–322, http://dx.doi.org/10.1016/b978-0-12-802999-2.00002-7

H. Yoshida, T. Watanabe, H. Kanamori, T. Yoshida, S. Ogiwara, K. Eguchi, Experimental study on water production by hydrogen reduction of lunar soil simulant in a fixed bed reactor, in: Space Resources Roundtable, 2000
```
